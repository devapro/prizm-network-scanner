### Prizm peers scanner

This is a simple script to scan the Prizm network for peers. It uses the `getPeers` API call to get a list of peers from a node and then connects to each of them to get their peers. It then repeats this process for each peer it finds. This allows it to build a list of all the peers in the network.

### Data structure

#### Required files

Prizm network requires a list of peers to connect to. This list is stored in the next files:

peers_ssl.txt - list of relevant peers with SSL
peers.txt - list of relevant peers without SSL

These files updates after finishing process of scanning the network.

#### Temporary files

tmp_peers_ssl.txt - list of all unique peers with SSL after request peers from each node from peers_ssl.txt
tmp_peers.txt - list of all unique peers without SSL after request peers from each node from peers.txt

raw directory - contains raw data from each node

### Usage

source ./venv/bin/activate
pip install -r ./requierements.txt
python3 main.py