import os

from peer_check import check_peer_ssl, check_peer
from process_raw_results import process_raw_results, filter_unique_peers, save_active_peers_to_file


def move_file(source, destination):
    with open(source) as file_source:
        lines = file_source.readlines()
        with open(destination, 'w') as file_destination:
            file_destination.writelines(lines)
            with open(source, 'w') as f:
                f.writelines('')


def process_ssl_peers():
    with open('./data/peers_ssl.txt') as file:
        relevant_peers = []
        unique_lines = set(file.readlines())
        for line in unique_lines:
            peer = line.rstrip()
            print(peer)
            if check_peer_ssl(peer):
                relevant_peers.append(peer)
        file.close()
        with open('./data/peers_ssl.txt', 'w') as f:
            f.writelines(i + '\n' for i in relevant_peers)


def process_tmp_ssl_peers():
    with open('./data/tmp_peers_ssl.txt') as file:
        with open('./data/peers_ssl.txt', 'a') as f:
            lines = file.readlines()
            size_of_lines = len(lines)
            for index, line in enumerate(lines):
                print(f'{index}/{size_of_lines}')
                peer = line.rstrip()
                if check_peer_ssl(peer):
                    f.write(peer + '\n')
                    f.flush()
                    print(peer)


def process_peers():
    with open('./data/peers.txt') as file:
        relevant_peers = []
        unique_lines = set(file.readlines())
        for line in unique_lines:
            peer = line.rstrip()
            if check_peer(peer):
                relevant_peers.append(peer)
                print(peer)
        file.close()
        with open('./data/peers.txt', 'w') as f:
            f.writelines(i + '\n' for i in relevant_peers)


def process_tmp_peers():
    with open('./data/tmp_peers.txt') as file:
        with open('./data/peers.txt', 'a') as f:
            lines = file.readlines()
            size_of_lines = len(lines)
            for index, line in enumerate(lines):
                print(f'{index}/{size_of_lines}')
                peer = line.rstrip()
                if check_peer(peer):
                    f.write(peer + '\n')
                    f.flush()
                    print(peer)


def process_raw_files():
    path = "raw"
    dir_list = os.listdir(path)
    for file in dir_list:
        process_raw_results(path + '/' + file)
        save_active_peers_to_file(path + '/' + file)


if __name__ == '__main__':
    # check all peers from peers_ssl.txt + fetch their peers
    process_ssl_peers()
    process_peers()
    # process raw responses + create tmp list of IPs for checking
    process_raw_files()
    # save only unique peers from tmp_peers_ssl.txt
    filter_unique_peers('data/tmp_peers_ssl.txt')
    filter_unique_peers('data/tmp_peers.txt')
    # check all peers from tmp_peers_ssl.txt + fetch their peers
    move_file('data/peers_ssl.txt', 'data/_peers_ssl.txt')
    process_tmp_ssl_peers()
    move_file('data/peers.txt', 'data/_peers.txt')
    process_tmp_peers()
