import json

from peer_check import check_peer_ssl, check_peer


def save_peer_to_file(file_name, peer):
    with open(file_name, "a") as file:
        file.write(peer)


def process_results(file):
    with open(file) as json_file:
        json_data = json.load(json_file)
        peers = json_data['peers']
        print(file)
        print(len(peers))
        for peer in peers:
            if 'apiSSLPort' in peer:
                if check_peer_ssl(peer['address'] + ':' + peer['apiSSLPort']):
                    save_peer_to_file('data/tmp_peers_ssl.txt', peer['address'] + ':' + str(peer['apiSSLPort']) + '\n')
                    print(peer['address'] + ':' + str(peer['apiSSLPort']))
                else:
                    if check_peer(peer['address'] + ':' + peer['port']):
                        save_peer_to_file('data/tmp_peers.txt', peer['address'] + ':' + str(peer['port']) + '\n')
                        print(peer['address'] + ':' + str(peer['port']))
