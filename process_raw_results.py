import json


def save_peer_to_file(file_name, peer):
    with open(file_name, "a") as file:
        file.write(peer)


def process_raw_results(path):
    with open(path) as json_file:
        try:
            json_data = json.load(json_file)
        except Exception as e:
            print(e)
            return
        try:
            peers = json_data['peers']
            print(path)
            print(len(peers))
            for peer in peers:
                if 'apiSSLPort' in peer:
                    save_peer_to_file('data/tmp_peers_ssl.txt', peer['address'] + ':' + str(peer['apiSSLPort']) + '\n')
                else:
                    save_peer_to_file('data/tmp_peers.txt', peer['address'] + ':' + str(peer['port']) + '\n')
        except Exception as e:
            print(e)


def filter_unique_peers(path):
    with open(path) as file:
        unique_lines = set(file.readlines())
        with open(path, 'w') as f:
            f.writelines(unique_lines)


def save_active_peers_to_file(path):
    with open(path) as json_file:
        try:
            json_data = json.load(json_file)
        except Exception as e:
            print(e)
            return
        try:
            peers = json_data['peers']
            print(path)
            print(len(peers))
            for peer in peers:
                print(peer['lastUpdated'])
                if peer['lastUpdated'] > 175216541:
                    save_peer_to_file('data/active_peers.txt', peer['address'] + '\n')
        except Exception as e:
            print(e)
    filter_unique_peers('data/active_peers.txt')
