import ssl

import requests
from urllib3.exceptions import InsecureRequestWarning


def saveToFile(file_name, content):
    with open(file_name, 'w') as file:
        file.write(content)


def save_peer_to_file(file_name, peer):
    with open(file_name, "a") as file:
        file.write(peer)


def check_peer_ssl(peer):
    try:
        requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE

        session = requests.Session()
        session.verify = False
        session.trust_env = False
        response = session.post('https://' + peer + '/prizm', data={'requestType': 'getPeers', 'includePeerInfo': 'true'},
                                verify=False, timeout=5, allow_redirects=True)
        json_response = response.text
        saveToFile('raw/' + peer + '.txt', json_response)
        return True
    except Exception as e:
        print(e)
        return False


def check_peer(peer):
    try:
        requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE

        session = requests.Session()
        session.verify = False
        session.trust_env = False
        response = session.post('http://' + peer + '/prizm', data={'requestType': 'getPeers'},
                                verify=False, timeout=5, allow_redirects=True)
        json_response = response.text
        saveToFile('raw/' + peer + '.txt', json_response)
        return True
    except Exception as e:
        print(e)
        return False
